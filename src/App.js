import React from "react";
import Layout from "./components/Layout";
import { FaLinkedin, FaGitlab, FaGithub } from "react-icons/fa";
function App() {
  return (
    <Layout>
      <div className="p-4 flex items-baseline border-b-2 border-black">
        <h1 className="text-6xl pb-4">Davood Rafiee</h1>
        <h2 className="text-2xl pl-2">Web Developer</h2>
      </div>
      <ul className="p-4 flex text-xl font-bold">
        <li className="p-2">
          <a href="https://ir.linkedin.com/in/davood-rafiee-0594ab130">
            <FaLinkedin fill="#0a66c2" size={50} />
          </a>
        </li>
        <li className="p-2">
          <a href="https://gitlab.com/davoodfox">
            <FaGitlab fill="#fc6d26" size={50} />
          </a>
        </li>
        <li className="p-2">
          <a href="https://github.com/davoodfox">
            <FaGithub fill="#24292f" size={50} />
          </a>
        </li>
      </ul>
    </Layout>
  );
}

export default App;
